import Settings from './../settings/Settings';


class SensorNetStore {

    static get() {
        return [
            {
                lastModified: 1493675851852,
                networkId: "networkId-31d4bcb9-b9e2-4967-9330-8fe36ba70e9e",
                collectorId: "collectorId-83c28765-94fe-4b03-9baa-3c2a9326bde2",
                storageTimestampInMillis: 1493675851851,
                captureTimestampInMillis: 1490734268440,
                acquisitionTimestampInMillis: 1490734285277,
                capturePrecisionInNano: 1490734,
                consumables: {
                    "Good Signal": 11,
                    "Low Signal": 69,
                    "Low battery": 13,
                    "Full battery": 58,
                    "Normal battery": 46,
                    "Normal Signal": 92
                },
                values: [
                    {
                      name: "temperature",
                      type: "NUMBER",
                      value: 26612,
                      unit: "26612",
                      symbol: "K"
                    }
                ],
                id: "sensorId-c613aa7f-7456-45cf-bbfc-844708c9eb41",
                state: "NEW",
                info: {
                    motelMode: "automatic",
                    parent: "parent sensor"
                }
            },
            {
                lastModified: 1493675851852,
                networkId: "networkId-31d4bcb9-b9e2-4967-9330-8fe36ba70e9e",
                collectorId: "collectorId-83c28765-94fe-4b03-9baa-3c2a9326bde2",
                storageTimestampInMillis: 1493675851852,
                captureTimestampInMillis: 1490734268440,
                acquisitionTimestampInMillis: 1490734285277,
                capturePrecisionInNano: 1490734,
                consumables: {
                    "Good Signal": 11,
                    "Low Signal": 69,
                    "Low battery": 13,
                    "Full battery": 58,
                    "Normal battery": 46,
                    "Normal Signal": 92
                },
                values: [
                    {
                      name: "temperature",
                      type: "NUMBER",
                      value: 26612,
                      unit: "26612",
                      symbol: "K"
                    }
                ],
                id: "sensorId-c613aa7f-7456-45cf-bbfc-844708c9eb4",
                state: "NEW",
                info: {
                    motelMode: "automatic",
                    parent: "parent sensor"
                }
            },
            {
                lastModified: 1493675851852,
                networkId: "networkId-31d4bcb9-b9e2-4967-9330-8fe36ba70e9e",
                collectorId: "collectorId-83c28765-94fe-4b03-9baa-3c2a9326bde2",
                storageTimestampInMillis: 1493675851852,
                captureTimestampInMillis: 1490734268440,
                acquisitionTimestampInMillis: 1490734285277,
                capturePrecisionInNano: 1490734,
                consumables: {
                    "Good Signal": 11,
                    "Low Signal": 69,
                    "Low battery": 13,
                    "Full battery": 58,
                    "Normal battery": 46,
                    "Normal Signal": 92
                },
                values: [
                    {
                      name: "temperature",
                      type: "NUMBER",
                      value: 26612,
                      unit: "26612",
                      symbol: "K"
                    }
                ],
                id: "sensorId-c613aa7f-7456-45cf-bbfc-844708c9eb45",
                state: "NEW",
                info: {
                    motelMode: "automatic",
                    parent: "parent sensor"
                }
            },{
                lastModified: 1493675851852,
                networkId: "networkId-31d4bcb9-b9e2-4967-9330-8fe36ba70e9e",
                collectorId: "collectorId-83c28765-94fe-4b03-9baa-3c2a9326bde2",
                storageTimestampInMillis: 1493675851852,
                captureTimestampInMillis: 1490734268440,
                acquisitionTimestampInMillis: 1490734285277,
                capturePrecisionInNano: 1490734,
                consumables: {
                    "Good Signal": 11,
                    "Low Signal": 69,
                    "Low battery": 13,
                    "Full battery": 58,
                    "Normal battery": 46,
                    "Normal Signal": 92
                },
                values: [
                    {
                      name: "temperature",
                      type: "NUMBER",
                      value: 26612,
                      unit: "26612",
                      symbol: "K"
                    }
                ],
                id: "sensorId-c613aa7f-7456-45cf-bbfc-844708c9eb47",
                state: "NEW",
                info: {
                    motelMode: "automatic",
                    parent: "parent sensor"
                }
            },{
                lastModified: 1493675851852,
                networkId: "networkId-31d4bcb9-b9e2-4967-9330-8fe36ba70e9e",
                collectorId: "collectorId-83c28765-94fe-4b03-9baa-3c2a9326bde2",
                storageTimestampInMillis: 1493675851852,
                captureTimestampInMillis: 1490734268440,
                acquisitionTimestampInMillis: 1490734285277,
                capturePrecisionInNano: 1490734,
                consumables: {
                    "Good Signal": 11,
                    "Low Signal": 69,
                    "Low battery": 13,
                    "Full battery": 58,
                    "Normal battery": 46,
                    "Normal Signal": 92
                },
                values: [
                    {
                      name: "temperature",
                      type: "NUMBER",
                      value: 26612,
                      unit: "26612",
                      symbol: "K"
                    }
                ],
                id: "sensorId-c613aa7f-7456-45cf-bbfc-844708c9eb49",
                state: "NEW",
                info: {
                    motelMode: "automatic",
                    parent: "parent sensor"
                }
            }
        ];
    }

}

export default SensorNetStore;
