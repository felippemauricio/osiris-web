import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';
import Login from './components/login/Login';
import Base from './components/base-layout/Base';
import MainMenu from './components/main-menu/MainMenu';
import SensorNet from './components/sensor-net/SensorNet';
import VirtualSensorNet from './components/virtual-sensor-net/VirtualSensorNet';


const REACT_START_DIV = 'osiris';


ReactDOM.render(
    <Router history={ browserHistory }>
        <Route path="/login" component={ Login } />
        <Route path="/" component={ Base }>
            <IndexRoute component={ MainMenu }/>
            <Route path="/sensornet" component={ SensorNet }/>
            <Route path="/virtualsensornet" component={ VirtualSensorNet }/>
        </Route>
    </Router>,
    document.getElementById(REACT_START_DIV)
);
