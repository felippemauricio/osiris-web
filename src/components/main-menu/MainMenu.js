import React, { Component } from 'react';
import Counts from './Counts';


class MainMenu extends Component {

    render() {
        return (
            <section>
                <Counts />    
            </section>
        );
    }

}

export default MainMenu;