import React, { Component } from 'react';
import SensorNetStore from './../../store/SensorNetStore';
import Loading from './../utils/Loading';
import Table from './../table/Table';


class SensorNet extends Component {

    constructor() {
        super();
        this.state = {
            sensors: [],
            reading: true
        };
    }

    _getTableHead() {
        return [
            {
                id: 'id',
                text: 'Id'
            },
            {
                text: 'NetworkId',
                id: 'networkId'
            },
            {
                text: 'CollectorId',
                id: 'collectorId'
            },
            {
                text: 'storageTimestampInMillis',
                id: 'storageTimestampInMillis'
            },
            {
                text: 'captureTimestampInMillis',
                id: 'captureTimestampInMillis'
            },
            {
                text: 'capturePrecisionInNano',
                id: 'capturePrecisionInNano'
            }
        ];
    }

    componentDidMount() {
        this.setState({
            sensors: SensorNetStore.get(),
            reading: false
        });
    }

    render() {
        return (
            <section className="row">
                <div className="col-md-12 col-sm-12 col-xs-12">
                    <div className="x_panel">
                        <div className="x_title">
                            <h2>SensorNet <Loading show={ this.state.reading } /></h2>
                            <div className="clearfix"></div>
                        </div>
                        <Table thead={ this._getTableHead() } tbody={ this.state.sensors } />
                    </div>
                </div>
            </section>
        );
    }

}

export default SensorNet;