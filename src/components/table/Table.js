import React, { Component } from 'react';


class Table extends Component {

    render() {
        return (
            <div className="x_content">
                <table id="datatable-fixed-header" className="table table-striped table-bordered">
                    <thead>
                        <tr>{ this.props.thead.map(headTitle => <th>{ headTitle.text }</th>) }</tr>
                    </thead>
                    <tbody>
                        {
                            this.props.tbody.map(element => {
                                <tr key={ element.id }>
                                    { this.props.thead.map(headTitle => <td>{ headTitle.text }</td>) }
                                </tr>
                            }) 
                        }
                    </tbody>
                </table>
            </div>
        );
    }

}

export default Table;