class Settings {

    static NAME_SYSTEM = 'Osiris Web';
    static DEVELOPERS = [
        'André Mendes Ghigo',
        'Felippe Maurício Vieira'
    ];
    static API_ENDPOINT = 'http://localhost:8080';

}

export default Settings;